<?php
/*
 * Memanggil semua file yang ada di folder BusinessLogic
 * Memanggil semua file yang ada di folder Model
 * */

require_once 'BusinessLogic/RemoveTodoList.php';
require_once 'BusinessLogic/AddTodoList.php';
require_once 'BusinessLogic/ShowTodoList.php';
require_once 'Model/TodoList.php';
require_once 'View/ViewRemoveTodoList.php';
require_once 'View/ViewAddTodoList.php';
require_once 'View/ViewShowTodoList.php';

echo "Aplikasi Todo List" . PHP_EOL;

?>