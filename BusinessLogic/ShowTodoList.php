<?php

/*
 * Menampilkan Todo List
 * */

function showTodoList() {
    global $todoList;

    echo "Todo List" . PHP_EOL;

    foreach ($todoList as $todo => $value) {
        echo $todo . " ".  $value . PHP_EOL;
    }

}